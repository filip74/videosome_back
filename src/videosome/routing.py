from channels.routing import route_class
from channels.generic.websockets import WebsocketDemultiplexer

from .websockets import VideosomeMultiplexer
from chat.consumers import ChatConsumer
from rooms.consumers import RoomConsumer


class Demultiplexer(WebsocketDemultiplexer):
    multiplexer_class = VideosomeMultiplexer

    consumers = {
        'chat': ChatConsumer,
        'room': RoomConsumer,
    }


channel_routing = [
    route_class(
        Demultiplexer,
        path=r'^/stream/(?P<room_id>[a-zA-Z0-9]{3,10})/',
    ),
]
