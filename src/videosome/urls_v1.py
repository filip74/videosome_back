from django.conf.urls import (
    include,
    url,
)

import djoser.views
from rest_framework import routers

import auth_ex.views
import rooms.views

router = routers.DefaultRouter()

router.register(
    r'rooms',
    rooms.views.RoomViewSet,
)

urlpatterns = [
    url(r'^login/$', djoser.views.LoginView.as_view()),
    url(r'^sign_up/$', djoser.views.RegistrationView.as_view()),
    url(r'^me/$', auth_ex.views.MeView.as_view()),
    url(
        r'^room_state/(?P<room_id>[a-zA-Z0-9]{3,10})/$',
        rooms.views.RoomStateView.as_view(),
    ),
    url(r'', include(router.urls)),
]
