from functools import partialmethod

from channels.generic.websockets import (
    JsonWebsocketConsumer,
    WebsocketMultiplexer,
)

from .utils import RestTokenConsumerMixin


class VideosomeMultiplexer(WebsocketMultiplexer):

    MESSAGE_STATUSES = {
        'success': 'SUCCESS',
        'error': 'ERROR',
    }
    MESSAGE_TYPES = {
        'connect': 'CONNECT',
        'receive': 'RECEIVE',
        'disconnect': 'DISCONNECT',
    }

    def group_send(
        self,
        message_status,
        message_type,
        group_name,
        payload=None,
    ):
        if payload is None:
            payload = {}

        # defeinsive programming FTW
        assert 'status' not in payload
        assert 'type' not in payload

        WebsocketMultiplexer.group_send(
            group_name,
            self.stream,
            {
                'status': message_status,
                'type': message_type,
                **payload,
            },
        )

    def send(self, message_status, message_type, payload=None):
        if payload is None:
            payload = {}

        # defeinsive programming FTW
        assert 'status' not in payload
        assert 'type' not in payload

        super().send({
            'status': message_status,
            'type': message_type,
            **payload,
        })

    group_send_connect_success = partialmethod(
        group_send,
        MESSAGE_STATUSES['success'],
        MESSAGE_TYPES['connect'],
    )

    group_send_connect_error = partialmethod(
        group_send,
        MESSAGE_STATUSES['error'],
        MESSAGE_TYPES['connect'],
    )

    group_send_receive_success = partialmethod(
        group_send,
        MESSAGE_STATUSES['success'],
        MESSAGE_TYPES['receive'],
    )

    group_send_receive_error = partialmethod(
        group_send,
        MESSAGE_STATUSES['error'],
        MESSAGE_TYPES['receive'],
    )

    group_send_disconnect_success = partialmethod(
        group_send,
        MESSAGE_STATUSES['success'],
        MESSAGE_TYPES['disconnect'],
    )

    # TODO: check if can disonnect ever fail
    group_send_disconnect_error = partialmethod(
        group_send,
        MESSAGE_STATUSES['error'],
        MESSAGE_TYPES['disconnect'],
    )

    send_connect_success = partialmethod(
        send,
        MESSAGE_STATUSES['success'],
        MESSAGE_TYPES['connect'],
    )

    send_connect_error = partialmethod(
        send,
        MESSAGE_STATUSES['error'],
        MESSAGE_TYPES['connect'],
    )

    send_receive_success = partialmethod(
        send,
        MESSAGE_STATUSES['success'],
        MESSAGE_TYPES['receive'],
    )

    send_receive_error = partialmethod(
        send,
        MESSAGE_STATUSES['error'],
        MESSAGE_TYPES['receive'],
    )

    send_disconnect_success = partialmethod(
        send,
        MESSAGE_STATUSES['success'],
        MESSAGE_TYPES['disconnect'],
    )

    # TODO: check if can disonnect ever fail
    send_disconnect_error = partialmethod(
        send,
        MESSAGE_STATUSES['error'],
        MESSAGE_TYPES['disconnect'],
    )
