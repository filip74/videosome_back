from rest_framework import generics
from rest_framework.authtoken.models import Token

from .serializers import TokenSerializer


class MeView(generics.RetrieveAPIView):
    serializer_class = TokenSerializer
    model = Token

    def get_object(self):
        return self.request.user.auth_token
