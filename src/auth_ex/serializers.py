from django.contrib.auth.password_validation import validate_password

from rest_framework import serializers
from rest_framework.authtoken.models import Token

from djoser.serializers import (
    UserRegistrationSerializer as DjoserUserRegistrationSerializer,
    UserSerializer,
)


class TokenSerializer(serializers.ModelSerializer):
    """Serializer used to create successful login response.

    Attributes:
        auth_token (serializers.CharField): Granted token
        user (djoser.serializers.UserSerializer): Authenticated user data
    """

    auth_token = serializers.CharField(source='key')
    user = UserSerializer()

    class Meta:
        model = Token
        fields = (
            'auth_token',
            'user',
        )


class UserRegistrationSerializer(DjoserUserRegistrationSerializer):

    def validate_password(self, value):
        """Validate password using Django password validators."""
        validate_password(value)
        return value
