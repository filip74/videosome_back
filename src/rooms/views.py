from rest_framework import (
    generics,
    permissions,
    viewsets,
)

from .cache import RoomStateCache
from .models import Room
from .serializers import (
    RoomSerializer,
    RoomStateSerializer,
)
from videosome.permissions import IsOwnerOrReadOnly
from videosome.utils import get_video_id


class RoomViewSet(viewsets.ModelViewSet):
    permission_classes = (
        permissions.IsAuthenticated,
        IsOwnerOrReadOnly,
    )
    serializer_class = RoomSerializer
    queryset = Room.objects.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
        RoomStateCache(serializer.instance.pk).save_room_state({
            'video_id': get_video_id(serializer.validated_data['video_url']),
            'owner_id': self.request.user.pk,
        })


class RoomStateView(generics.RetrieveAPIView):
    serializer_class = RoomStateSerializer
    lookup_url_kwarg = 'room_id'

    def get_object(self):
        room_id = self.kwargs[self.lookup_url_kwarg or self.lookup_field]
        return RoomStateCache(room_id).get_room_state()
