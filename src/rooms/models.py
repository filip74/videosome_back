from django.conf import settings
from django.core.validators import (
    MinLengthValidator,
    RegexValidator,
)
from django.db import models
from django.utils.translation import ugettext_lazy as _

from videosome.utils import short_uuid


class Room(models.Model):
    name = models.CharField(
        default=short_uuid,
        max_length=10,
        primary_key=True,
        unique=True,
        validators=[
            MinLengthValidator(3),
            RegexValidator(
                regex=r'[a-zA-Z0-9]{3,10}',
                message=_(
                    'Room name can only contain latin letters and numbers.'
                ),
            ),
        ],
    )
    title = models.CharField(
        blank=True,
        max_length=50,
    )
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='owned_rooms',
    )

    def save(self, *args, **kwargs):
        # short uuid with length of 10 has a pretty high chance of collision
        # in the long run
        while self.__class__.objects.filter(pk=self.pk).exists():
            self.pk = short_uuid()
        super().save(*args, **kwargs)
