from django.core.cache import cache

from .serializers import RoomStateSerializer


class RoomStateCache:
    prefix = 'room_state_'

    def __init__(self, room_id):
        self._cache_key = '{}{}'.format(self.prefix, room_id)

    def get_room_state(self):
        return cache.get(self._cache_key, {})

    def save_room_state(self, room_state):
        instance = self.get_room_state()
        if instance:
            serializer = RoomStateSerializer(
                instance,
                data=room_state,
            )
        else:
            serializer = RoomStateSerializer(
                data=room_state,
            )
        serializer.is_valid(raise_exception=True)
        cache.set(self._cache_key, serializer.validated_data)
