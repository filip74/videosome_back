from channels.generic.websockets import JsonWebsocketConsumer

from .cache import RoomStateCache
from .models import Room
from videosome.utils import (
    get_video_id,
    RestTokenConsumerMixin,
)


def get_room_group_name(room_id):
    return f'room_{room_id}'


YOUTUBE_PLAYER_EVENTS = (
    'error',
    'unplayable',
    'timeupdate',
    'unstarted',
    'ended',
    'playing',
    'paused',
    'buffering',
    'cued',
    'playbackQualityChange',
    'playbackRateChange',
)
UPDATE_ROOM_STATE_EVENT = 'UPDATE_ROOM_STATE'
CHANGE_VIDEO_EVENT = 'CHANGE_VIDEO'


class RoomConsumer(RestTokenConsumerMixin, JsonWebsocketConsumer):
    rest_user = True

    def connection_groups(self, multiplexer, room_id):
        return [get_room_group_name(room_id)]

    def connect(self, message, multiplexer, room_id):
        multiplexer.send_connect_success()

    def receive(self, payload, multiplexer, room_id):
        room_state_cache = RoomStateCache(room_id)

        # if cached state for the room has not yet ever been saved, current
        # state will be empty dict, so get owner id from db
        owner_id = room_state_cache.get_room_state().get(
            'owner_id',
            Room.objects.get(pk=room_id).owner_id,
        )

        # only room owner's messages should be published to other room members
        if owner_id != self.message.user.pk:
            return

        if payload.get('event') == UPDATE_ROOM_STATE_EVENT:
            self.handle_room_state_update(
                payload,
                room_state_cache,
            )
        elif payload.get('event') in YOUTUBE_PLAYER_EVENTS:
            self.handle_player_event(
                payload,
                multiplexer,
                room_id,
            )
        elif payload.get('event') == CHANGE_VIDEO_EVENT:
            self.handle_change_video(
                payload,
                multiplexer,
                room_id,
                room_state_cache,
            )

    def disconnect(self, message, multiplexer, room_id):
        multiplexer.send_disconnect_success()

    def handle_room_state_update(self, payload, room_state_cache):
        room_state_cache.save_room_state({
            **payload,
            'owner_id': self.message.user.pk,
        })

    def handle_player_event(self, payload, multiplexer, room_id):
        multiplexer.group_send_receive_success(
            get_room_group_name(room_id),
            payload,
        )

    def handle_change_video(
        self,
        payload,
        multiplexer,
        room_id,
        room_state_cache,
    ):
        video_id = get_video_id(payload['payload']['video_url'])
        room_state_cache.save_room_state({
            'video_id': video_id,
            'owner_id': self.message.user.pk,
        })
        multiplexer.group_send_receive_success(
            get_room_group_name(room_id),
            {
                'event': CHANGE_VIDEO_EVENT,
                'payload': {
                    'video_id': video_id,
                },
            },
        )
