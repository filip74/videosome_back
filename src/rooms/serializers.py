from copy import deepcopy

from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

from .models import Room


class RoomSerializer(serializers.ModelSerializer):

    class Meta:
        model = Room
        fields = (
            'name',
            'title',
            'owner',
            'video_url',
        )
        read_only_fields = (
            'owner',
        )

    video_url = serializers.CharField(write_only=True)

    def get_fields(self):
        fields = super().get_fields()
        pk_field_name = Room._meta.pk.name

        # if the instance exists, make its pk field readonly
        if (
            isinstance(self.instance, Room) and
            self.instance.pk
        ):
            fields[pk_field_name].read_only = True

        return fields

    def create(self, validated_data):
        data = deepcopy(validated_data)
        data.pop('video_url')
        return super().create(data)


class RoomStateSerializer(serializers.Serializer):
    STATE_PAUSED = 'paused'
    STATE_PLAYING = 'playing'
    STATE_ENDED = 'ended'
    STATE_BUFFERING = 'buffering'
    STATE_UNSTARTED = 'unstarted'
    STATE_CHOICES = (
        (STATE_PAUSED, _('Paused')),
        (STATE_PLAYING, _('Playing')),
        (STATE_ENDED, _('Ended')),
        (STATE_BUFFERING, _('Buffering')),
        (STATE_UNSTARTED, _('Unstarted')),
    )

    video_id = serializers.CharField()
    time = serializers.IntegerField(default=0)  # seconds from video start
    state = serializers.ChoiceField(
        choices=STATE_CHOICES,
        default=STATE_PAUSED,
    )
    owner_id = serializers.IntegerField()
