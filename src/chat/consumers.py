from datetime import datetime

from channels.generic.websockets import JsonWebsocketConsumer

from videosome.utils import RestTokenConsumerMixin


def get_chat_group_name(room_id):
    return f'chat_{room_id}'


class ChatConsumer(RestTokenConsumerMixin, JsonWebsocketConsumer):
    rest_user = True

    def connection_groups(self, multiplexer, room_id):
        return [get_chat_group_name(room_id)]

    def connect(self, message, multiplexer, room_id):
        multiplexer.send_connect_success()

    def receive(self, payload, multiplexer, room_id):
        user = self.message.user
        if not user or 'message' not in payload:
            # TODO: distinguish those two cases and maybe add some error codes?
            multiplexer.send_receive_error()
            return
        multiplexer.group_send_receive_success(
            get_chat_group_name(room_id),
            {
                'message': payload['message'],
                'author': {
                    'username': user.username,
                    'id': user.id,
                },
                'timestamp': datetime.now().isoformat(),
            },
        )

    def disconnect(self, message, multiplexer, room_id):
        multiplexer.send_disconnect_success()
